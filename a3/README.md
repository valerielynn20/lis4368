> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Valerie Smith

### Assignment 3 # Requirements:

*Sub-Heading:*

1. Entity Relationship Diagram
2. Include data (at least 10 records each table)
3. Provide Bitbucket read-only access to repo. Must include README.md using Markdown syntax, and include links to all of the files

This is a blockquote.

This is the second paragraph in the blockquote.


#### Assignment Screenshot and Links:*Screenshot A3 ERD*:
*Screenshot of AMPPS running https://localhost*:

![A3 ERD] (img/a3.png) 

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb)

[A3 SQL File](docs/a3.sql)

### Tutorial Links:

*Bitbucket Repository:*
[LIS4368 Bitbucket Repository] (https://bitbucket.org/valerielynn20/lis4368/)