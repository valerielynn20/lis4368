> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 - Advanced Web Applications

## Valerie Smith

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials 
      (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL
    - Create database ebookshop
    - Web page that executes "CRUD" operations
    - Provide screenshots of running servlets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create tables using MySQL Workbench and forward-engineer locally
    - Entry Relationship Diagram
    - Include at least 10 records per table

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Model View Controller (MVC)
    - Add form controls to match attributes of customer entity
    - Provide screenshots of failed and passed validation
    
    

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Model View Controller (MVC)
    - Divide an application's implementation into three discrete component roles 
    - Design and deploy data-driven applications

6. [P1 README.md](p1/README.md "My A6 README.md file")
    - Add form controls to match attributes of customer entity
    - Add jQuery validation, as per the entity attribute requirements (except for notes)
    - Provide screenshots of failed and passed validation

7. [P2 README.md](a7/README.md "My A7 README.md file")

