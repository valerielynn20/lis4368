> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Valerie Smith

### Assignment 5 # Requirements:

*Sub-Heading:*
- Model View Controller (MVC)
- Divide an application's implementation into three discrete component roles: models, views, and controllers 
- Design and deploy data-driven applications

#### Assignment Screenshot:

![Screenshot of failed entries](img/cusform.png) 

![Screenshot of working entries](img/thanks.png)

![Screenshot of working entries](img/database.png)

