> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Valerie Smith

### Assignment 2 # Requirements:

*Sub-Heading*

1. Web page that executes "CRUD" operations 
2. MySQL Installation
3. Chapter Questions (Chs5-6)

#### README.md file should include the following items:

* Screenshot of running (http://localhost:9999/hello)
* Screenshot of running (http://localhost:9999/hello/index.html)
* git commands w/short descriptions;
* Bitbucket repo links: a)this assignment and b)the completed tutorial above (bitbucketstationlocations)


#### Assignment 2 Links:

*A2 lis 4368:*
[A2 Lis 4368](http://localhost:9999/lis4368/ "A2 Lis 4368")

*A2 say hi:*
[A2 Say hi](http://localhost:9999/hello/sayhi "A2 Say hi")

*A2 querybook.html:*
[A2 querybook.html](http://localhost:9999/hello/querybook.html "querybook.html")


#### Assignment 2 Pictures:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/JDK.png)
