> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Valerie Smith

### Assignment 4 # Requirements:

*Sub-Heading:*
- Model View Controller (MVC)
- Add form controls to match attributes of customer entity 
- Provide screenshots of failed and passed validation

#### Assignment Screenshot:

![Screenshot of failed entries](img/failed.png) 

![Screenshot of working entries](img/pass.png)

