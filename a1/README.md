> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Valerie Smith

### Assignment_1 # Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket 
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of running https://localhost9999 (#2 above, Step #4(b) in tutorial);
* git commands w/short descriptions;
* Bitbucket repo links: a)this assignment and b)the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- Turns a directory into an empty Git repository
2. git status- Returns the current state of the repository 
3. git add- Adds files into the staging area for Git
4. git commit- Record the changes made to the files to a local repository
5. git push- Sends local commits to the remote repository
6. git pull- Get the lastest version of a repository
7. git clone- Create a local working copy of an existing remote repository

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/JDK.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/valerielynn20/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/valerielynn20/myteamquotes/ "My Team Quotes Tutorial")
