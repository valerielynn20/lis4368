> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Valerie Smith

### Project 1 # Requirements:

*Sub-Heading:*
1. Add form controls to match attributes of customer entity
2. Add jQuery validation, as per the entity attribute requirements (except for notes) 
3. Provide screenshots of failed and passed validation

#### Project Screenshot and Links:

![Screenshot of failed entries](img/p1failed.png) 

![Screenshot of working entries](img/p1work.png)

